<?php

$clientName = check_input($_POST['client_name'], "Please enter name");
$clientEmail = check_input($_POST['client_email'], "Please enter your email");
$clientPhone = check_input($_POST['client_phone'], "Please enter your phone number");
$clientKosher = check_input($_POST['client_kosher'], "Please check yes or no for kosher");
$clientDesc = check_input($_POST['client_desc'], "Please enter your message");
//functions we used

function check_input($data, $problem='')
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    if ($problem && strlen($data) == 0)
    {
        alert("Just click the back button to return to sponsors! But before you do, please fix the following issue in the form submission:    " + die($problem));
    }
    return $data;
}

//preparing message for email:
$message = "Hello Sean, here is a form submission from the contact page!
Contact Information (Name, Email, Phone): 
$clientName, $clientEmail, $clientPhone.

Kosher? $clientKosher
Message:

$clientDesc.

-----

    
Please let me know if you want any message changed or improved upon
Smaze
End of message";

$myEmail = "commonwealthcatering1@gmail.com";
$subject = "Form Submission commonwealthcatering | $name";

//sending message using mail function
mail($myEmail, $subject, $message);

//sending confirmation to user
mail($clientEmail, "From us at Commonwealth Catering, Thank you!", "From us at Commonwealth Catering, thank you for choosing us as your catering service provider! This is a message confirmation to thank you for contacting us through our website, We will respond as quick as we can at your earliest convienience!");
//sending users back to contact page
header('Location: http://commonwealthcateringky.com/contact.html');
?>